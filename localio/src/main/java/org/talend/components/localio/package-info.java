@Icon(FLOW_O)
@Components(family = "LocalIO")
package org.talend.components.localio;

import org.talend.sdk.component.api.component.Components;
import org.talend.sdk.component.api.component.Icon;

import static org.talend.sdk.component.api.component.Icon.IconType.FLOW_O;
