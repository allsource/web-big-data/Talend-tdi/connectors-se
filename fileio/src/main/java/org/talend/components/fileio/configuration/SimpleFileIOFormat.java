package org.talend.components.fileio.configuration;

public enum SimpleFileIOFormat {
    CSV,
    EXCEL,
    // JSON,
    AVRO,
    PARQUET
}
