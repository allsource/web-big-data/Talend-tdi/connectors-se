package org.talend.components.fileio.configuration;

public enum ExcelFormat {

    EXCEL2007,
    EXCEL97,
    HTML

}
