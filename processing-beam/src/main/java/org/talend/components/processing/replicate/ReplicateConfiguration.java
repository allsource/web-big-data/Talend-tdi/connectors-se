package org.talend.components.processing.replicate;

import lombok.Data;
import org.talend.sdk.component.api.meta.Documentation;

import java.io.Serializable;

@Data
@Documentation("ReplicateConfiguration, empty for the moment.")
public class ReplicateConfiguration implements Serializable {

}
